GPX Simplifier / Optimizer online
============

Online Simplifier and Optimizer for GPX tracks.

Copyright 2014 [Stefano Cudini](http://labs.easyblog.it/stefano-cudini/)

**Demo:**  
[labs.easyblog.it/maps/gpx-simplify-optimizer](http://labs.easyblog.it/maps/gpx-simplify-optimizer/)

![Image](https://raw.githubusercontent.com/stefanocudini/gpx-simplify-optimizer/master/images/gpx-optimizer.png)


Build

**compress files:**
```
npm install
grunt
```

**update libs:**
```
git submodule foreach git pull
```
